# postgis

The `postgis` image provides a Docker container running Postgres 9.4 with
[PostGIS 2.1](http://postgis.net/docs/manual-2.1/) installed. This image is
based on the mdillon / [`postgis`](https://registry.hub.docker.com/u/mdillon/postgis/)
image.

The PostGIS extension can be installed into your database in 
[the standard way](http://postgis.net/docs/postgis_installation.html#create_new_db_extensions) via `psql`:

```SQL
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_topology;
```

## Usage

In order to run a basic container capable of serving a PostGIS-enabled database,
start a container as follows:

    docker run --name some-postgis -e MAX_CONNECTIONS=400 -d near/postgis

For more detailed instructions about how to start and control your Postgres
container, see the documentation for the `postgres` image
[here](https://registry.hub.docker.com/_/postgres/).

Once you have started a database container, you can then connect to the
database as follows:

    docker run -it --link some-postgis:postgres --rm postgres \
        sh -c 'exec psql -h "$POSTGRES_PORT_5432_TCP_ADDR" -p "$POSTGRES_PORT_5432_TCP_PORT" -U postgres'

Using the resulting `psql` shell, you can create a PostGIS-enabled database by
using the `CREATE EXTENSION` mechanism:

```SQL
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_topology;
```

See [the PostGIS documentation](http://postgis.net/docs/postgis_installation.html#create_new_db_extensions)
for more details on your options for creating and using a spatially-enabled database.

